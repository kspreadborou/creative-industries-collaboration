# Creative Industries Collaboration
**Resources**

Useful Links:
* An introduction and overview to Canvas API: https://community.canvaslms.com/docs/DOC-14390-canvas-apis-getting-started-the-practical-ins-and-outs-gotchas-tips-and-tricks#jive_content_id_Overview
* Canvas UAT (test site): https://unimelb-uat.instructure.com/login/canvas
* API fields, Beta (can view, but need API token to get, delete, ect): unimelb.beta.instructure.com/doc/api/live
* API fielfd, Live (can view, but need API token to get, delete, ect): unimelb.instructure.com/doc/api/live
* Canvas API FAQs: https://community.canvaslms.com/docs/DOC-8513-canvas-data-faq
* Canvas LMS API Documentation: https://canvas.instructure.com/doc/api/index.html

Packages:
* canvasapi, a Python library for accessing Instructure’s Canvas LMS API: https://github.com/ucfopen/canvasapi

