#This code will connect you to the Canvas API. You can use it for the live and  UAT (test) Canvas sites, but the content has to be published for it to be findable via API (might not work on UAT?)
#It uses the canvasapi package (https://github.com/ucfopen/canvasapi)
#If you don't already have an API key/token, you will need to generate one (https://community.canvaslms.com/docs/DOC-14409-4214861717)

#Install the canvasAPI package
pip install canvasapi

# Import the Canvas class
from canvasapi import Canvas

# Canvas API URL
API_URL = "https://canvas.lms.unimelb.edu.au/"

# Canvas API key
API_KEY = "AddYourKey"

# Initialize a new Canvas object
canvas = Canvas(API_URL, API_KEY)

#Now, using the canvasapi package, you can call info that you have access to in your Canvas account using canvas.xxxx. Check out https://canvasapi.readthedocs.io/en/stable/ for info